# Common locator file for all locators
# coding: utf-8

import json
import os

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


nexus5x_data_file = ROOT_DIR + '/conf/Nexus5x.json'
nexus5X_test_bed = {}
with open(nexus5x_data_file) as json_data_file:
    nexus5X_test_bed = json.load(json_data_file)
# ========== Adding swipe related keys here for Nexus5x device start ====== #
nexus5X_test_bed["swipe_up"] = (400, 300, 400, 700, 400)
nexus5X_test_bed["swipe_right"] = (800, 700, 400, 700, 400)
nexus5X_test_bed["swipe_down"] = (100, 700, 100, 400, 500)
# ========== Adding swipe related keys here for Nexus5x device end ====== #


nexus6_data_file = ROOT_DIR + '/conf/Nexus6.json'
nexus6_test_bed = {}
with open(nexus6_data_file) as json_data_file:
    nexus6_test_bed = json.load(json_data_file)
# ========== Adding swipe related keys here for Nexus5x device start ====== #
nexus6_test_bed["swipe_down"] = (400, 1600, 400, 200, 400)
nexus6_test_bed["swipe_up"] = (400, 400, 400, 1600, 400)
nexus6_test_bed["swipe_right"] = (1000, 1100, 400, 1100, 400)
nexus6_test_bed["swipe_message"] = (1000, 2000, 1000, 1800, 500)
# ========== Adding swipe related keys here for Nexus5x device end ====== #


pixel2_data_file = ROOT_DIR + '/conf/Pixel2.json'
Pixel2_test_bed = {}
with open(pixel2_data_file) as json_data_file:
    Pixel2_test_bed = json.load(json_data_file)
# ========== Adding swipe related keys here for pixel2 device start ====== #
Pixel2_test_bed["swipe_up"] = (400, 300, 400, 700, 400)
Pixel2_test_bed["swipe_right"] = (800, 700, 400, 700, 400)
Pixel2_test_bed["swipe_down"] = (100, 700, 100, 400, 500)
Pixel2_test_bed["swipe_message"] = (750, 1650, 750, 1300, 500)
# ========== Adding swipe related keys here for pixel2 device end ====== #


pixel_data_file = ROOT_DIR + '/conf/Pixel.json'
Pixel_test_bed = {}
with open(pixel_data_file) as json_data_file:
    Pixel_test_bed = json.load(json_data_file)
# ========== Adding swipe related keys here for pixel device start ====== #
Pixel_test_bed["swipe_up"] = (400, 300, 400, 700, 400)
Pixel_test_bed["swipe_right"] = (800, 700, 400, 700, 400)
Pixel_test_bed["swipe_down"] = (100, 700, 100, 400, 500)
Pixel_test_bed["swipe_message"] = (400, 1600, 400, 1200, 500)

# ========== Adding swipe related keys here for pixel device end ====== #

pixel_data_file = ROOT_DIR + '/conf/PixelXL.json'
Pixel_test_bed = {}
with open(pixel_data_file) as json_data_file:
    PixelXL_test_bed = json.load(json_data_file)
# ========== Adding swipe related keys here for pixelXL device start ====== #
PixelXL_test_bed["swipe_up"] = (400, 300, 400, 700, 400)
PixelXL_test_bed["swipe_right"] = (800, 700, 400, 700, 400)
PixelXL_test_bed["swipe_down"] = (100, 700, 100, 400, 500)
PixelXL_test_bed["swipe_message"] = (400, 1600, 400, 1200, 500)

# ========== Adding swipe related keys here for pixelXL device end ====== #

samsung_data_file = ROOT_DIR + '/conf/SamsungS9.json'
SamsungS9_test_bed = {}
with open(samsung_data_file) as json_data_file:
    SamsungS9_test_bed = json.load(json_data_file)
# ========== Adding swipe related keys here for pixel2 device start ====== #
SamsungS9_test_bed["swipe_up"] = (400, 300, 400, 700, 400)
SamsungS9_test_bed["swipe_right"] = (800, 700, 400, 700, 400)
SamsungS9_test_bed["swipe_down"] = (100, 700, 100, 400, 500)
SamsungS9_test_bed["swipe_message"] = (750, 1650, 750, 1300, 500)
# ========== Adding swipe related keys here for pixel2 device end ====== #

