"""Automation Class: execute DAT116 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
import subprocess
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction


class VerifyContentSharing(unittest.TestCase):
    """Implementation of DAT116 testcase to
     Verify Content sharing with app login."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + '/verify_content_sharing_DAT116'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_content_sharing(self):

        """Testcase to Verify Content sharing with app login."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        time.sleep(2)

        self.page.write(
            '=== Testcase Verify Content Sharing with app login '
            'Started at %s ===' % time.strftime("%H:%M:%S"))
        time.sleep(2)

        self.page.writeTableHeader(
            'DAT116 - Verify Content sharing with app login.')

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)


        self.page.driver.hide_keyboard()

        self.page.write('Clicking on Create or Join room button')
        self.page.click_mobile_element(
            self.current_bed.get("create_room"))

        self.page.write('Entering new sharing Name ')
        self.page.set_mobile_text("SharingRoom",
                                  self.current_bed.get("create_room"))

        self.page.write('Clicking on Create button')
        self.page.click_mobile_element(
            self.current_bed.get("create_button"))

        time.sleep(2)

        self.go_back(2)

        subprocess.Popen(
            'adb shell am start -W -n com.google.android.youtube/com.google.android.youtube.HomeActivity',
            shell=True)
        # self.page.driver.start_activity('com.google.android.youtube',
        #                                 'com.google.android.youtube.HomeActivity')
        time.sleep(5)

        self.page.write(
            'Checking "YouTube" App launched or not.')
        self.page.writeToTable(
            'Checking "YouTube" App launched or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("youtube_home"))
        if not is_present:
            self.page.writeToTable(
                '"YouTube" App is not launched.',
                None, None, self.error)
        else:
            self.page.write('"YouTube" App is launched.')
            self.page.writeToTable('"YouTube" App is launched.')

        self.page.write(
            'Checking "YouTube" Menu is available or not.')
        self.page.writeToTable(
            'Checking "YouTube" Menu is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("youtube_menu"))
        if not is_present:
            self.page.writeToTable(
                '"YouTube" Menu is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(self.current_bed.get("youtube_menu"))
            self.page.write('Clicked "YouTube" Menu.')
            self.page.writeToTable('Clicked "YouTube" Menu.')
        except NoSuchElementException:
            self.page.write('Unable to click "YouTube" Menu.')
            self.page.writeToTable('Unable to click "YouTube" Menu.',
                                   None, None, self.error)
            return False

        self.page.write(
            'Checking "Share" Button is available or not.')
        self.page.writeToTable(
            'Checking "Share" Button is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("youtube_share"))
        if not is_present:
            self.page.writeToTable(
                '"Share" Button is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("youtube_share"))
            self.page.write('Clicked on "Share" Button.')
            self.page.writeToTable('Clicked on "Share" Button.')
        except NoSuchElementException:
            self.page.write('Unable to click "Share" Button.')
            self.page.writeToTable('Unable to click "Share" Button.',
                                   None, None, self.error)
            return False

        time.sleep(2)

        self.page.write(
            'Checking "All Sharing Apps" is available or not.')
        self.page.writeToTable(
            'Checking "All Sharing Apps" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("youtube_all_app_share"))
        if not is_present:
            self.page.writeToTable(
                '"All Sharing Apps" name is not found.',
                None, None, self.error)
        else:
            self.page.write('"All Sharing Apps" name is found.')
            self.page.writeToTable('"All Sharing Apps" name is found.')

        self.page.write(
            'Checking "dynamite chat App" is available or not.')
        self.page.writeToTable(
            'Checking "dynamite chat App" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("chat_app"))
        if not is_present:
            self.page.writeToTable(
                '"dynamite chat App" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("chat_app"))
            self.page.write('Clicked on "dynamite chat App".')
            self.page.writeToTable('Clicked on "dynamite chat App".')
        except NoSuchElementException:
            self.page.write('Unable to click "dynamite chat App" .')
            self.page.writeToTable('Unable to click "dynamite chat App" .',
                                   None, None, self.error)
            return False

        self.page.write(
            'Checking "Sharing Room" is available or not.')
        self.page.writeToTable(
            'Checking "Sharing Room" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("sharing_room"))
        if not is_present:
            self.page.writeToTable(
                '"Sharing Room" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("sharing_room"))
            self.page.write('Clicked on "Sharing Room".')
            self.page.writeToTable('Clicked on "Sharing Room".')
        except NoSuchElementException:
            self.page.write('Unable to click "Sharing Room" .')
            self.page.writeToTable('Unable to click "Sharing Room" .',
                                   None, None, self.error)
            return False

        self.page.driver.hide_keyboard()

        self.page.write(
            'Checking "New conversation" window is available or not.')
        self.page.writeToTable(
            'Checking "New conversation" window is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("new_conversation"))
        if not is_present:
            self.page.writeToTable(
                '"New conversation" window is not found.',
                None, None, self.error)
        else:
            self.page.write('"New conversation" window is found.')
            self.page.writeToTable('"New conversation" window is found.')

        self.page.write(
            'Checking "url" in compose text field is available or not.')
        self.page.writeToTable(
            'Checking "url" in compose text field is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("hint_text"))
        if not is_present:
            self.page.writeToTable(
                '"url" in compose text feild is not found.',
                None, None, self.error)
        else:
            self.page.write('"url" in compose text feild window is found.')
            self.page.writeToTable(
                '"url" in compose text feild window is found.')


        self.page.write(
            'Checking "Arrow button" is available or not.')
        self.page.writeToTable(
            'Checking "Arrow button" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("post_message"))
        if not is_present:
            self.page.writeToTable(
                '"Arrow button" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("post_message"))
            self.page.write('Clicked on "Arrow button".')
            self.page.writeToTable('Clicked on "Arrow button".')
        except NoSuchElementException:
            self.page.write('Unable to click "Arrow button" .')
            self.page.writeToTable('Unable to click "Arrow button" .',
                                   None, None, self.error)
            return False

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_sharing_screen")
            first_image = self.golden_images + '/verify_sharing_screen.png'
            second_image = self.tc_dir_path + '/verify_sharing_screen.png'
            verify_login = self.images.compare_images(
                first_image, second_image, 'verify_sharing_screen',
                self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify youtube sharing room screen verfied successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Verify youtube sharing room screen verfied successfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/verify_sharing_screen' +
                    'Modified.png')
            else:
                self.page.write(
                    'Verify youtube sharing room screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify youtube sharing room screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/verify_sharing_screen' +
                    'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_sharing_screen')

        self.page.write(
            'Checking respective "Room" is available or not.')
        self.page.writeToTable(
            'Checking respective "Room" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("sharing_room"))
        if not is_present:
            self.page.writeToTable(
                'respective "Room" is not found.',
                None, None, self.error)
        else:
            self.page.write('respective "Room" is found.')
            self.page.writeToTable(
                'respective "Room" is found.')

        self.page.write(
            'Checking "Close button" is available or not.')
        self.page.writeToTable(
            'Checking "Close button" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("navigate_back"))
        if not is_present:
            self.page.writeToTable(
                '"Close button" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("navigate_back"))
            self.page.write('Clicked on "Close button".')
            self.page.writeToTable('Clicked on "Close button".')
        except NoSuchElementException:
            self.page.write('Unable to click "Close button" .')
            self.page.writeToTable('Unable to click "Close button" .',
                                   None, None, self.error)
            return False

        self.page.write(
            'Checking Today Header is available or not.')
        self.page.writeToTable(
            'Checking Today Header is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("today_header"))
        if not is_present:
            self.page.writeToTable(
                'Today Header is not found.',
                None, None, self.error)
        else:
            self.page.write('Today Header is found.')
            self.page.writeToTable(
                'Today Header is found.')

        self.page.write(
            'Checking "User avatar" is available or not.')
        self.page.writeToTable(
            'Checking "User avatar" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("user_avatar"))
        if not is_present:
            self.page.writeToTable(
                '"User avatar" is not found.',
                None, None, self.error)
        else:
            self.page.write('"User avatar" is found.')
            self.page.writeToTable(
                '"User avatar" is found.')

        self.page.write(
            'Checking "User Name" is available or not.')
        self.page.writeToTable(
            'Checking "User Name" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("user_name"))
        if not is_present:
            self.page.writeToTable(
                '"User Name" is not found.',
                None, None, self.error)
        else:
            self.page.write('"User Name" is found.')
            self.page.writeToTable(
                '"User Name" is found.')

        self.page.write(
            'Checking "User Timestamp" is available or not.')
        self.page.writeToTable(
            'Checking "User Timestamp" is available or not.')
        is_present = self.page.check_element_present(
            self.current_bed.get("timestamp"))
        if not is_present:
            self.page.writeToTable(
                '"User Timestamp" is not found.',
                None, None, self.error)
        else:
            self.page.write('"User Timestamp" is found.')
            self.page.writeToTable(
                '"User Timestamp" is found.')

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_sharing_world_view_screen")
            first_image = self.golden_images \
                          + '/verify_sharing_world_view_screen.png'
            second_image = self.tc_dir_path \
                           + '/verify_sharing_world_view_screen.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_sharing_world_view_screen', self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify youtube sharing World View room screen verfied '
                    'successfully. no major changes observed.')
                self.page.writeToTable(
                    'Verify youtube sharing World View room'
                    'screen verfied successfully. No major changes observed.',
                    first_image,
                    self.tc_dir_path + '/verify_sharing_world_view_screen'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Verify youtube sharing World View room screen'
                    'verification failed. Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify youtube sharing World View room screen verification'
                    ' failed. Major changes observed.', first_image,
                    self.tc_dir_path + '/verify_sharing_world_view_screen'
                    + 'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_sharing_world_view_screen')

        self.page.write('Clicking on Navigate back button')
        self.page.click_mobile_element(
            self.current_bed.get("navigate_back"))

        self.page.write('=== DAT116 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            'Test case DAT116 executed successfully in, %d Seconds'
            % (int(time.time() - start_time)))

    def go_back(self, rotate):
        """function to navigate back"""
        self.page.write('Going "' + str(rotate) + '" Step back')
        for _ in range(rotate):
            self.page.driver.back()



def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(VerifyContentSharing('test_verify_content_sharing'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())