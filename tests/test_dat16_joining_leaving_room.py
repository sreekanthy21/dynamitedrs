"""Automation Class: execute DAT16 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction

class JoiningLeavingRoom(unittest.TestCase):
    """Implementation of DAT16 testcase to verify Joining and Leaving Room."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + \
                             '/verify_joining_leaving_room_DAT16'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_joining_leaving_room(self):

        """Testcase to Validate Joining and leaving Room."""
        self.page.driver.start_activity(self.app_package, self.app_activity)

        time.sleep(2)

        start_time = int(time.time())
        self.page.write(
            '=== Testcase DAT16 - Verify Joining and leaving Room Get Started at %s ==='
            % time.strftime("%H:%M:%S"))

        self.page.writeTableHeader(
            'DAT16 - Testcase to Joining and Leaving Room')


        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("browse_rooms"))
        if not is_present:
            self.page.writeToTable('"browse_rooms" is not found.',
                                   None, None, self.error)
            return False
        else:
            self.page.writeToTable('"browse_rooms" is found.')

        try:
            self.page.click_mobile_element(
                self.current_bed.get("browse_rooms"))
            self.page.writeToTable('Successfully clicked on "browse_rooms ".')
        except NoSuchElementException:
            self.page.write('Unable to click "browse_rooms"')
            self.page.writeToTable('Unable to click "browse_rooms"',
                                   None, None, self.error)
            return False

        time.sleep(2)

        self.page.driver.hide_keyboard()

        time.sleep(3)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png(
                "verify_browse_rooms_layout_dat16")
            first_image = self.golden_images + \
                          '/verify_browse_rooms_layout_dat16.png'
            second_image = self.tc_dir_path +\
                           '/verify_browse_rooms_layout_dat16.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_browse_rooms_layout_dat16', self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'verify Creating room screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'verify Creating room screen verfied succsfully. '
                    'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_browse_rooms_layout_dat16' + 'Modified.png',)
            else:
                self.page.write(
                    'verify Creating room screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'verify Creating room screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path +
                    '/verify_browse_rooms_layout_dat16'
                    + 'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images,'verify_browse_rooms_layout_dat16')
        
        time.sleep(2)
        
        is_present = self.page.check_element_present(
            self.current_bed.get("join_icon"))
        if not is_present:
            self.page.writeToTable('"Join icon" is not found.',
                                   None, None, self.error)
            return False

        else:
            self.page.click_mobile_element(self.current_bed.get("join_icon"))
            self.page.write('"Join icon" is available and clicked.')
            self.page.writeToTable('"Join icon and room" is available and '
                                   'clicked.')

        time.sleep(4)

        is_present = self.page.check_element_present(
            self.current_bed.get("leave_icon"))
        if not is_present:
            self.page.writeToTable('"leave icon " is not found.',
                                   None, None, self.error)

            return False

        else:
            self.page.click_mobile_element(self.current_bed.get("leave_icon"))
            self.page.write('"leave icon " is available and clicked')
            self.page.writeToTable('"leave icon and room" is available '
                                   'and clicked')



        self.page.write('=== DAT16 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT16 executed successfully in, %d Seconds' % (
            int(time.time()
                - start_time)))

    def go_back(self, rotate):
        """ function to navigate back"""
        self.page.write('Going "' + str(rotate) + '" Step back')
        for _ in range(rotate):
            self.page.driver.back()


def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(JoiningLeavingRoom('test_joining_leaving_room'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
