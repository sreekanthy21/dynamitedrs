"""Automation Class: execute DAT15 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction
from datetime import datetime


class VerifyCreatingNewRoom(unittest.TestCase):
    """Implementation of DAT15 testcase to verify Creating a new room."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root +\
                             '/verify_creating_new_room_DAT15'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_creating_new_room(self):

        """Testcase to Create a New Room ."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())

        self.page.write(
            '=== Testcase DAT15 - Create a New Room Started at %s ==='%
            time.strftime("%H:%M:%S"))
        time.sleep(3)

        self.page.writeTableHeader(
            'DAT15 - Testcase to Create a New Room')

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("create_room"))
        if not is_present:
            self.page.writeToTable('Create room is not present on '
                                   'the screen', None, None, self.error)
            
        else:
            self.page.writeToTable('Create room is present on '
                                   'the screen.')

        try:
            self.page.click_mobile_element(
                self.current_bed.get("create_room"))
            self.page.writeToTable('Successfully clicked on create room.')
            self.page.write('Successfully clicked on create room')
        except NoSuchElementException:
            self.page.write('Unable to click "Create room"')
            self.page.writeToTable('Unable to click "Create room"',
                                   None, None, self.error)

        time.sleep(2)

        self.page.driver.hide_keyboard()

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png(
                "verify_create_room_page_dat15")
            first_image = self.golden_images + \
                          '/verify_create_room_page_dat15.png'
            second_image = self.tc_dir_path + \
                           '/verify_create_room_page_dat15.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_create_room_page_dat15', self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify create room verified successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Verify create room  verified successfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path +
                    '/verify_create_room_page_dat15' + 'Modified.png')
            else:
                self.page.write(
                    'Verify create room verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify create room verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path +
                    '/verify_create_room_page_dat15' + 'Modified.png',
                    self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_create_room_page_dat15')


        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("create_room_page"))
        if not is_present:
            self.page.writeToTable(
                'Create room header is not found.', None, None, self.error)
        else:
            self.page.write('Create room header is found.')
            self.page.writeToTable('Create room header is found.')


        time.sleep(2)

        today = datetime.today().strftime('%d/%m/%y')
        is_present = self.page.check_element_present(
            self.current_bed.get("create_find_room"))
        if not is_present:
            self.page.writeToTable(
                '"Create room" text box is not found.',
                None, None, self.error)
            
        else:
            self.page.writeToTable(
                '"Create room" text box is present on the screen.')

        try:
            self.page.set_mobile_text(today,
                                      self.current_bed.get("create_find_room"))
            self.page.writeToTable(
                'Successfully entered text into "Create Room" text box')

        except NoSuchElementException:
            self.page.writeToTable(
                'Unable to enter text in "Create room" text box"',
                None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("create_button"))
        if not is_present:
            self.page.writeToTable('Create room button is not found.',
                                   None, None, self.error)
            
        else:
            self.page.writeToTable('Create room button is found.')

        try:
            self.page.click_mobile_element(
                self.current_bed.get("create_button"))
            self.page.write('Successfully clicked on create room button')
            self.page.writeToTable('Successfully clicked on create room button')
        except NoSuchElementException:
            self.page.write('Unable to click create room button')
            self.page.writeToTable('Unable to click create room button',
                                   None, None, self.error)

        time.sleep(2)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png(
                "verify_new_room_created_dat15")
            first_image = self.golden_images + \
                          '/verify_new_room_created_dat15.png'
            second_image = self.tc_dir_path + \
                           '/verify_new_room_created_dat15.png'
            verify_login = self.images.compare_images(
                first_image, second_image,
                'verify_new_room_created_dat15',
                self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write(
                    'Verify new room created '
                    'screen verified successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Verify new room created '
                    'screen verified successfully. '
                    'No major changes observed.', first_image, self.tc_dir_path
                    + '/verify_new_room_created_dat15' +
                    'Modified.png')
            else:
                self.page.write(
                    'Verify new room created '
                    'screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Verify new room created '
                    'Verify new room created'
                    'screen verification failed. '
                    'Major changes observed.', first_image, self.tc_dir_path
                    + '/verify_new_room_created_dat15' +
                    'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images,
                'verify_new_room_created_dat15')

        time.sleep(2)

        self.page.write('=== DAT15 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT15 executed successfully in, %d Seconds' %
            (int(time.time() - start_time)))


    def go_back(self, rotate):
        """function to navigate back"""
        self.page.write('Going "' + str(rotate) + '" Step back')
        for _ in range(rotate):
            self.page.driver.back()

    def scrol_up(self):
        """function to scroll down the screen"""
        scroll_up = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to down')
        self.page.driver.swipe(scroll_up[0], scroll_up[1], scroll_up[2],
                               scroll_up[3], scroll_up[4])
        time.sleep(2)



def suite():
    execute = unittest.TestSuite()
    execute.addTest(VerifyCreatingNewRoom('test_verify_creating_new_room'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
