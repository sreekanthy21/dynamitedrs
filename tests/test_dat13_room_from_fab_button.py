"""Automation Class: execute DAT13 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction


class RoomFromFabButton(unittest.TestCase):
    """Implementation of DAT13 testcase to Verify Add Room navigation """

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root +\
                             '/verify_room_from_fab_button_DAT13'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_room_from_fab_button(self):
        """Testcase to verify Add Room navigation from Fab Button ."""

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        self.page.write(
            '=== Testcase DAT13 - Verify Room from fab button Get Started at %s ==='
            %time.strftime("%H:%M:%S"))
        time.sleep(1)
        self.page.writeTableHeader(
            'DAT13 - Verify Room from Fab button Testcase')

        is_present = self.page.check_element_present(
            self.current_bed.get("fab_button"))
        if not is_present:
            self.page.writeToTable(
                '"fab_button"  is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"fab_button" is found.')
            self.page.write("fab_button  found")
        try:
            self.page.click_mobile_element(
                self.current_bed.get("fab_button"))
        except NoSuchElementException:
            self.page.write('Unable to click on fab_button')
            self.page.writeToTable('Unable to click on fab_button',
                                   None, None, self.error)

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("create_room"))

        if not is_present:
            self.page.writeToTable('"Create room" is not present on '
                                   'the screen', None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Create room" is present on '
                                   'the screen.')

        try:
            self.page.click_mobile_element(
                self.current_bed.get("create_room"))
            self.page.writeToTable('Successfully clicked on "Create '
                                   'room" ')
            self.page.write('Clicked on create room')
        except NoSuchElementException:
            self.page.write('Unable to click "Create room"')
            self.page.writeToTable('Failed to click "Create room"',
                                   None, None, self.error)
            return False

        time.sleep(3)

        self.page.driver.hide_keyboard()

        time.sleep(2)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("verify_create_room_layout_dat13")
            first_image = self.golden_images + '/verify_create_room_layout_dat13.png'
            second_image = self.tc_dir_path + '/verify_create_room_layout_dat13.png'
            verify_login = self.images.compare_images(
                first_image, second_image, 'verify_create_room_layout_dat13',
                self.tc_dir_path)

            score = float(round(verify_login, 1))
            if score >= 1:
                self.page.write('verify Add room screen verfied succsfully. '
                                'no major changes observed.')
                self.page.writeToTable(
                    'verify Add room screen verfied succsfully. '
                    'No major changes observed.', first_image,
                                       self.tc_dir_path +
                                       '/verify_create_room_layout_dat13' + 'Modified.png',)
            else:
                self.page.write('verify Add room screen verification failed. '
                                'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'verify Add room screen verification failed. '
                    'Major changes observed.', first_image, self.tc_dir_path +
                    '/verify_create_room_layout_dat13' + 'Modified.png', self.error)

        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'verify_create_room_layout_dat13')

        is_present = self.page.check_element_present(
            self.current_bed.get("create_room_page"))
        if not is_present:
            self.page.writeToTable(
                '"Create Room" name is not found.', None, None, self.error)
            return False
        else:
            self.page.write('"Create Room" name is found.')
            self.page.writeToTable('"Create Room" name is found.')

        self.page.driver.back()

        self.page.write('=== DAT13 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(' Test case DAT13 executed successfully in,'
                                   ' %d Seconds' % (int(time.time()
                                                        - start_time)))


def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(RoomFromFabButton('test_room_from_fab_button'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
