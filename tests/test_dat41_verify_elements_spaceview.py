"""Automation Class: execute DAT41 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException


class VerifyElementsSpaceView(unittest.TestCase):
    """Implementation of DAT41 testcase to Verify timestamps, date dividers
     and unread labels in space view."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' + \
                           conf.desired_caps.get('deviceName') + \
                           '_' + conf.desired_caps.get('platformVersion') + \
                           '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root + \
                             '/verify_elements_space_view_DAT41'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_elements_spaceview(self):

        self.page.driver.start_activity(self.app_package, self.app_activity)

        start_time = int(time.time())
        time.sleep(1)

        self.page.write('=== Testcase DAT41 - Verify Elements'
                        ' in space view Get Started at %s ===' %
                        time.strftime("%H:%M:%S"))
        time.sleep(1)
        self.page.writeTableHeader(
            'DAT41 - Verify Elements Spaceview ')

        self.scrol_down()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable(
                '"Find room or DM" search bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("DAT41 Room",
                                      self.current_bed.get("search_box"))
            self.page.write('Entering DAT41 Room into search box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Find room or DM" search '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Find room or DM" '
                'search bar"', None, None, self.error)
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("dat41_room"))
        if not is_present:
            self.page.writeToTable(
                'Dat41 Room is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('DAT41 Room is found.')
        try:
            self.page.click_mobile_element(
                self.current_bed.get("dat41_room"))
        except NoSuchElementException:
            self.page.write('Unable to click on DAT41 Room')
            self.page.writeToTable('Unable to click on DAT41 Room',
                                   None, None, self.error)

        time.sleep(2)

        if self.page.check_element_present(
                self.current_bed.get("new_topic")) is True:
            is_present = self.page.check_element_present(
                self.current_bed.get("new_topic"))
        else:
            is_present = self.page.check_element_present(
                self.current_bed.get("new_topic_menu"))

        if not is_present:
            self.page.writeToTable('"New Topic" is not found.',
                                   None, None, self.error)
            return False
    
        time.sleep(2)

        try:
            if self.page.check_element_present(
                    self.current_bed.get("new_topic")) is True:
                self.page.write('Clicking on New Topic')
                self.page.click_mobile_element(
                    self.current_bed.get("new_topic"))
            else:
                self.page.write('Clicking on New Topic')
                self.page.click_mobile_element(
                    self.current_bed.get("new_topic_menu"))
        except NoSuchElementException:
            self.page.write('Unable to click New Topic')
            self.page.writeToTable('Unable to click New Topic',
                                   None, None, self.error)
            return False

        self.page.driver.hide_keyboard()

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("set_message_41"))
        if not is_present:
            self.page.writeToTable(
                'Compose text bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('Compose text bar is found.')

        try:
            self.page.set_mobile_text("Hello",
                                      self.current_bed.get("set_message_41"))
            self.page.write('Entering Hello text into compose box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Reply text" compose bar '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Reply text" compose bar"',
                None, None, self.error)
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("post_message"))
        if not is_present:
            self.page.writeToTable(
                '"Arrow button" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("post_message"))
            self.page.write('Clicked on "Arrow button".')
        except NoSuchElementException:
            self.page.write('Unable to click "Arrow button" .')
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("navigate_back"))
        if not is_present:
            self.page.writeToTable(
                '"Close button" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("navigate_back"))
            self.page.write('Clicked on "Close button".')
        except NoSuchElementException:
            self.page.write('Unable to click "Close button" .')
            return False

        self.go_back(1)

        is_present = self.page.check_element_present(
            self.current_bed.get("navigation_menu"))
        if not is_present:
            self.page.writeToTable(
                '"navigation_menu" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("navigation_menu"))
            self.page.write('Clicked on "navigation_menu".')
        except NoSuchElementException:
            self.page.write('Unable to click "navigation_menu" .')

            return False

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("log_out"))
        if not is_present:
            self.page.writeToTable(
                '"log_out" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("log_out"))
            self.page.write('Clicked on "log_out".')
            self.page.writeToTable('Clicked on "log_out".')
        except NoSuchElementException:
            self.page.write('Unable to click "log_out" .')
            self.page.writeToTable('Unable to click "log_out" .',
                                   None, None, self.error)
            return False

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("sign_out"))
        if not is_present:
            self.page.writeToTable(
                '"sign_out" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("sign_out"))
            self.page.write('Clicked on "sign_out".')
            self.page.writeToTable('Clicked on "sign_out".')
        except NoSuchElementException:
            self.page.write('Unable to click "sign_out" .')
            self.page.writeToTable('Unable to click "sign_out" .',
                                   None, None, self.error)
            return False

        time.sleep(5)

        self.page.write('Clicking on Get started')
        self.page.click_mobile_element(self.current_bed.get("get_started"))

        self.page.write('Clicking on dynamite account')
        self.page.click_mobile_element(
            self.current_bed.get("dynamite_mail_1"))

        self.page.write('Clicking on OK button')
        self.page.click_mobile_element(self.current_bed.get("OK"))

        time.sleep(2)

        self.page.write('Clicking on Allow button')
        self.page.click_mobile_element(self.current_bed.get("allow_button"))

        self.go_back(2)
        print 'app activity launching'
        self.page.driver.start_activity(self.app_package,
                                        self.app_activity)

        time.sleep(2)

        self.pull_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable(
                '"Find room or DM" search bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("DAT41 Room",
                                      self.current_bed.get("search_box"))
            self.page.write('Entering DAT41 Room into search box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Find room or DM" search '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Find room or DM" '
                'search bar"', None, None, self.error)
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("dat41_room"))
        if not is_present:
            self.page.writeToTable(
                'Dat41 Room is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('DAT41 Room is found.')
        try:
            self.page.click_mobile_element(
                self.current_bed.get("dat41_room"))
        except NoSuchElementException:
            self.page.write('Unable to click on Dat41 Room')
            self.page.writeToTable('Unable to click on Dat41 Room',
                                   None, None, self.error)

        time.sleep(2)


        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout_dat41")
            first_image = self.golden_images + '/room_preview_layout_dat41.png'
            second_image = self.tc_dir_path + '/room_preview_layout_dat41.png'
            room_preview_layout_dat41 = self.images.compare_images(
                first_image, second_image, 'room_preview_layout_dat41',
                self.tc_dir_path)

            score = float(round(room_preview_layout_dat41, 1))
            if score >= 1:
                self.page.write(
                    'Space View Preview Layout screen verfied successfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied successfully. '
                    'No major changes observed.')
            else:
                self.page.write(
                    'Space View Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Space View Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat41' + 'Modified.png',
                    self.error)
        else:
            self.page.golden_image_screenshot(
                self.golden_images, 'room_preview_layout_dat41')

        is_present = self.page.check_element_present(
            self.current_bed.get("unread_room_message"))
        if not is_present:
            self.page.writeToTable(
                '"Unread" name is not found.', None, None, self.error)
        else:
            self.page.write('"Unread" name is found.')
            self.page.writeToTable('"Unread" name is found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("date_divider_room"))
        if not is_present:
            self.page.writeToTable(
                '"Date divider" is not found.', None, None, self.error)
        else:
            self.page.write('"Date divider" is found.')
            self.page.writeToTable('"Date divider" is found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("timestamp"))
        if not is_present:
            self.page.writeToTable(
                '"TimeStamp" is not found.', None, None, self.error)
        else:
            self.page.write('"Timestamp" is found.')
            self.page.writeToTable('"Timestamp" is found.')

        is_present = self.page.check_element_present(
            self.current_bed.get("user_avatar"))
        if not is_present:
            self.page.writeToTable(
                '"User avatar" is not found.', None, None, self.error)
        else:
            self.page.write('"User avatar" is found.')
            self.page.writeToTable('"User avatar" is found.')

        self.go_back(1)

        is_present = self.page.check_element_present(
            self.current_bed.get("navigation_menu"))
        if not is_present:
            self.page.writeToTable(
                '"navigation_menu" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("navigation_menu"))
            self.page.write('Clicked on "navigation_menu".')
        except NoSuchElementException:
            self.page.write('Unable to click "navigation_menu" .')

            return False

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("log_out"))
        if not is_present:
            self.page.writeToTable(
                '"log_out" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("log_out"))
            self.page.write('Clicked on "log_out".')
            self.page.writeToTable('Clicked on "log_out".')
        except NoSuchElementException:
            self.page.write('Unable to click "log_out" .')
            self.page.writeToTable('Unable to click "log_out" .',
                                   None, None, self.error)
            return False

        time.sleep(2)

        is_present = self.page.check_element_present(
            self.current_bed.get("sign_out"))
        if not is_present:
            self.page.writeToTable(
                '"sign_out" is not found.', None, None,
                self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("sign_out"))
            self.page.write('Clicked on "sign_out".')
            self.page.writeToTable('Clicked on "sign_out".')
        except NoSuchElementException:
            self.page.write('Unable to click "sign_out" .')
            self.page.writeToTable('Unable to click "sign_out" .',
                                   None, None, self.error)
            return False

        time.sleep(2)
        self.page.write('=== DAT41 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT41 executed successfully in, %d Seconds'
            % (int(time.time() - start_time)))

    def go_back(self, rotate):
        """Method to navigate back"""
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


    def pull_up(self):
        """method to pull up the screen"""
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)
    def scrol_down(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to down')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)


def suite():
    """automation package suite"""
    execute = unittest.TestSuite()
    execute.addTest(VerifyElementsSpaceView('test_verify_elements_spaceview'))
    return execute


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
