"""Automation Class: execute DAT61 testcase."""
# pylint: disable=import-error
import unittest
import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from conf import mobile_crd as conf
from page_objects.Mobile_Base_Page import Mobile_Base_Page
from page_objects.validate_images import ValidateImages
from selenium.common.exceptions import NoSuchElementException



class VerifyElementsTopicView(unittest.TestCase):
    """Implementation of DAT61 testcase to Verify Elements in topic view."""

    def setUp(self):
        self.current_bed = conf.current_test_bed
        self.page = Mobile_Base_Page()
        self.tc_dir_path = conf.screenshots_dir + '/' +\
                           conf.desired_caps.get('deviceName') +\
                          '_' + conf.desired_caps.get('platformVersion') +\
                          '_' + self.page.get_calling_module()
        self.golden_images = conf.golden_images_root \
                             + '/elements_topic_view_DAT61'
        self.images = ValidateImages()
        self.app_activity = conf.desired_caps['appActivity']
        self.app_package = conf.desired_caps['appPackage']
        self.error = '#ff0000'

    def test_verify_elements_topic_view(self):

        """Testcase to Verify Elements in topic view."""
        start_time = int(time.time())
        time.sleep(1)

        self.page.write(
            '=== Testcase Verify Elements in topic view Started at %s ===' %
            time.strftime("%H:%M:%S"))
        time.sleep(1)

        self.page.writeTableHeader(
            'DAT61 - Verify Elements in topic view ')

        self.page.driver.start_activity(self.app_package,
                                        self.app_activity)
        time.sleep(2)

        self.pull_up()

        is_present = self.page.check_element_present(
            self.current_bed.get("search_box"))
        if not is_present:
            self.page.writeToTable(
                '"Find room or DM" search bar is not found.',
                None, None, self.error)
            return False
        else:
            self.page.writeToTable('"Find room or DM" search bar is found.')

        try:
            self.page.set_mobile_text("Dat61",
                                      self.current_bed.get("search_box"))
            self.page.write('Entering "Dat61" Room into search box ')
        except NoSuchElementException:
            self.page.write(
                'Unable to input text in "Find room or DM" search '
                'bar"')
            self.page.writeToTable(
                'Unable to input text in "Find room or DM" '
                'search bar"', None, None, self.error)
            return False

        is_present = self.page.check_element_present(
            self.current_bed.get("dat61_room"))
        if not is_present:
            self.page.writeToTable(
                '"Dat61" Room is not found.',
                None, None, self.error)
        else:
            self.page.writeToTable('"Dat61" Room is found.')
        try:
            self.page.click_mobile_element(
                self.current_bed.get("dat61_room"))
        except NoSuchElementException:
            self.page.write('Unable to click on Dat61 Room')
            self.page.writeToTable('Unable to click on Dat61 Room',
                                   None, None, self.error)

        time.sleep(3)

        if conf.execution_type == 'automation':
            self.page.save_screenshot_png("room_preview_layout_dat61")
            first_image = self.golden_images + '/room_preview_layout_dat61.png'
            second_image = self.tc_dir_path + '/room_preview_layout_dat61.png'
            room_preview_layout_dat61 = self.images.compare_images(
                first_image, second_image, 'room_preview_layout_dat61',
                self.tc_dir_path)

            score = float(round(room_preview_layout_dat61, 1))
            if score >= 1:
                self.page.write(
                    'Room Preview Layout screen verfied succsfully. '
                    'no major changes observed.')
                self.page.writeToTable(
                    'Room Preview Layout screen verfied succsfully. '
                    'No major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat61'
                    + 'Modified.png')
            else:
                self.page.write(
                    'Room Preview Layout screen verification failed. '
                    'Major changes observed.', 'CRITICAL')
                self.page.writeToTable(
                    'Room Preview Layout screen verification failed. '
                    'Major changes observed.', first_image,
                    self.tc_dir_path + '/room_preview_layout_dat61'
                    + 'Modified.png', self.error)
        else:
            self.page.golden_image_screenshot(self.golden_images,
                                              'room_preview_layout_dat61')

        is_present = self.page.check_element_present(
            self.current_bed.get("user_name"))
        if not is_present:
            self.page.writeToTable(
                '"User name" is not found.',
                None, None, self.error)
        else:
            self.page.write('"User name" is found.')
            self.page.writeToTable('"User name" is found.')

        is_present = self.page.check_element_present(
            self.current_bed.get("timestamp"))
        if not is_present:
            self.page.writeToTable(
                '"Timestamp" is not found.',
                None, None, self.error)
        else:
            self.page.write('"Timestamp" is found.')
            self.page.writeToTable('"Timestamp" is found.')

        is_present = self.page.check_element_present(
            self.current_bed.get("user_avatar"))
        if not is_present:
            self.page.writeToTable(
                '"User avatar" is not found.',
                None, None, self.error)
        else:
            self.page.write('"User avatar" is found.')
            self.page.writeToTable('"User avatar" is found.')

        is_present = self.page.check_element_present(
            self.current_bed.get("edited_tag"))
        if not is_present:
            self.page.writeToTable(
                '"Edited tag" is not found.',
                None, None, self.error)
        else:
            self.page.write('"Edited tag" is found.')
            self.page.writeToTable('"Edited tag" is found.')


        is_present = self.page.check_element_present(
            self.current_bed.get("reply_button"))
        if not is_present:
            self.page.writeToTable(
                '"Reply" Button is not found.', None, None, self.error)
            return False
        try:
            self.page.click_mobile_element(
                self.current_bed.get("reply_button"))
        except NoSuchElementException:
            self.page.write(
                'Unable to click "Reply" Button.')
            self.page.writeToTable(
                'Unable to click "Reply" Button.',
                None, None, self.error)
            return False

        self.page.driver.hide_keyboard()


        is_present = self.page.check_element_present(
            self.current_bed.get("reply_text"))
        if not is_present:
            self.page.writeToTable(
                '"Compose bar" is not found.',
                None, None, self.error)
        else:
            self.page.write('"Compose bar" is found.')
            self.page.writeToTable('"Compose bar" is found.')

        self.go_back(2)


        self.page.write('=== DAT61 - ended at %s ===' %
                        time.strftime("%H:%M:%S"))
        self.page.write('Script duration: %d seconds\n' % (int(time.time()
                                                               - start_time)))
        self.page.writeTableFooter(
            ' Test case DAT61 executed successfully in, %d Seconds'
            % (int(time.time()- start_time)))

    def go_back(self, rotate):
        self.page.write('Going "' + str(rotate) + '" Step back')
        for i in range(rotate):
            self.page.driver.back()


    def pull_up(self):
        scroll_down = self.current_bed.get("swipe_up")
        self.page.write('Screen scrolling to up')
        self.page.driver.swipe(scroll_down[0], scroll_down[1], scroll_down[2],
                               scroll_down[3], scroll_down[4])
        time.sleep(2)



def suite():
    suite = unittest.TestSuite()
    suite.addTest(VerifyElementsTopicView('test_verify_elements_topic_view'))
    return suite


# ---START OF SCRIPT
if __name__ == '__main__':
    unittest.TextTestRunner(failfast=True).run(suite())
